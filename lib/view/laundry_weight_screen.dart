import 'package:flutter/material.dart';

class LaundryWeightScreen extends StatefulWidget {
  final String type;
  final int price;
  final int duration;
  const LaundryWeightScreen({ Key? key, required this.type, required this.price, required this.duration }) : super(key: key);

  @override
  State<LaundryWeightScreen> createState() => _LaundryWeightScreenState();
}

class _LaundryWeightScreenState extends State<LaundryWeightScreen> {
  final TextEditingController weightController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Text(
                  'Input Your Laundry Weight',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 36
                  ),
                ),
              ),
              TextFormField(
                controller: weightController,
                decoration: InputDecoration(
                  hintText: "Laundry Weight (kg)",
                  hintStyle: TextStyle(color: Colors.black
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                    color: Colors.black,
                    width: 2.0
                  )
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                    color: Colors.black, 
                    width: 2.0
                  )
                ),
              ),
            )
            ],
          ),
        )
      ),
    );
  }
}