import 'package:flutter/material.dart';

class WashingMachinesScreen extends StatefulWidget {
  final String type;
  final int price;
  final int duration;
  const WashingMachinesScreen({ Key? key, required this.type, required this.price, required this.duration }) : super(key: key);

  @override
  State<WashingMachinesScreen> createState() => _WashingMachinesScreenState();
}

class _WashingMachinesScreenState extends State<WashingMachinesScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.all(16),
          child: Column(
            children: [
              Center(
                child: Text(
                  widget.type,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 36
                  ),
                ),
              ),
              Center(
                child: Text(
                  '${widget.duration} day, Rp ${widget.price}/kg',
                  style: TextStyle(
                    fontSize: 18
                  ),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 4),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(5, (index) => InkWell(
                  onTap: () {
    
                  },
                  child: Image.asset(
                    'assets/img/washing_machine.png',
                    height: 60,
                    width: 60,
                  ),
                )),
              ),
              SizedBox(height: 26),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(5, (index) => InkWell(
                  onTap: () {
    
                  },
                  child: Image.asset(
                    'assets/img/washing_machine.png',
                    height: 60,
                    width: 60,
                  ),
                )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}